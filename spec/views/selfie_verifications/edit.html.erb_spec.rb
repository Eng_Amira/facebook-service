require 'rails_helper'

RSpec.describe "selfie_verifications/edit", type: :view do
  before(:each) do
    @selfie_verification = assign(:selfie_verification, SelfieVerification.create!(
      :user_id => 1,
      :note => "MyString"
    ))
  end

  it "renders the edit selfie_verification form" do
    render

    assert_select "form[action=?][method=?]", selfie_verification_path(@selfie_verification), "post" do

      assert_select "input[name=?]", "selfie_verification[user_id]"

      assert_select "input[name=?]", "selfie_verification[note]"
    end
  end
end
