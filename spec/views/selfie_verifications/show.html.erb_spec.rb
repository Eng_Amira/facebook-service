require 'rails_helper'

RSpec.describe "selfie_verifications/show", type: :view do
  before(:each) do
    @selfie_verification = assign(:selfie_verification, SelfieVerification.create!(
      :user_id => 2,
      :note => "Note"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Note/)
  end
end
