require 'rails_helper'

RSpec.describe "nationalid_verifications/edit", type: :view do
  before(:each) do
    @nationalid_verification = assign(:nationalid_verification, NationalidVerification.create!(
      :user_id => 1,
      :legal_name => "MyString",
      :national_id => 1,
      :type => 1,
      :status => 1,
      :note => "MyString"
    ))
  end

  it "renders the edit nationalid_verification form" do
    render

    assert_select "form[action=?][method=?]", nationalid_verification_path(@nationalid_verification), "post" do

      assert_select "input[name=?]", "nationalid_verification[user_id]"

      assert_select "input[name=?]", "nationalid_verification[legal_name]"

      assert_select "input[name=?]", "nationalid_verification[national_id]"

      assert_select "input[name=?]", "nationalid_verification[type]"

      assert_select "input[name=?]", "nationalid_verification[status]"

      assert_select "input[name=?]", "nationalid_verification[note]"
    end
  end
end
