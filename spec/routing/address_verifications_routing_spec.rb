require "rails_helper"

RSpec.describe AddressVerificationsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/address_verifications").to route_to("address_verifications#index")
    end

    it "routes to #new" do
      expect(:get => "/address_verifications/new").to route_to("address_verifications#new")
    end

    it "routes to #show" do
      expect(:get => "/address_verifications/1").to route_to("address_verifications#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/address_verifications/1/edit").to route_to("address_verifications#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/address_verifications").to route_to("address_verifications#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/address_verifications/1").to route_to("address_verifications#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/address_verifications/1").to route_to("address_verifications#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/address_verifications/1").to route_to("address_verifications#destroy", :id => "1")
    end
  end
end
