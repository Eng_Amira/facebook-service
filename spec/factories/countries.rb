FactoryBot.define do
    factory :country do
        short_code { Faker::Lorem.word }
        Phone_code { Faker::Number.number(3) }
        Full_Name { Faker::Lorem.word }
        Currency { Faker::Lorem.word }
        language { Faker::Lorem.word }
        active 1 
    end
end