class RenameCWatchdogAdminId < ActiveRecord::Migration[5.2]
  def change
    rename_column :watchdogs, :admin_id, :user_id
  end
end
