class DeleteGoogleSecretFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :google_secret
  end
end
