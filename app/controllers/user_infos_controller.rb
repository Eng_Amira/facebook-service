class UserInfosController < ApplicationController
  before_action :set_user_info, only: [:show, :edit, :update, :destroy]
  before_action :require_login

  # show details of Verification infos for a specific user
  # @param [Integer] id 
  # @return [String] username
  # @return [String] mobile
  # @return [Integer] address_verification_id
  # @return [Integer] nationalid_verification_id
  # @return [Integer] selfie_verification_id
  # @return [Integer] Status
  # @return [datetime] created_at
  # @return [datetime] updated_at 
  def show
    @user_infos = UserInfo.where("id =?", params[:id]).includes(:selfie_verification, :nationalid_verification, :address_verification)
    if current_user.id != @user_info.user_id.to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end
  
  def create
    @user_info = UserInfo.new(user_info_params)

    respond_to do |format|
      if @user_info.save
        format.html { redirect_to @user_info, notice: 'User info was successfully created.' }
        format.json { render :show, status: :created, location: @user_info }
      else
        format.html { render :new }
        format.json { render json: @user_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # edit User Info status
  # @param [Integer] id
  # @param [String] mobile
  # @param [Integer] address_verification_id
  # @param [Integer] nationalid_verification_id
  # @param [Integer] selfie_verification_id
  # @param [Integer] nationalid_verification_id
  # @param [Integer] status  
  # @return [Integer] id
  # @return [String] mobile
  # @return [Integer] address_verification_id
  # @return [Integer] nationalid_verification_id
  # @return [Integer] selfie_verification_id
  # @return [Integer] Status
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    respond_to do |format|
      if @user_info.update(user_info_params)
        format.html { redirect_to @user_info, notice: 'User info was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_info }
      else
        format.html { render :edit }
        format.json { render json: @user_info.errors, status: :unprocessable_entity }
      end
    end
  end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_info
      @user_info = UserInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_info_params
      params.require(:user_info).permit(:user_id, :mobile, :address_verification_id, :nationalid_verification_id, :selfie_verification_id, :status)
    end
end
