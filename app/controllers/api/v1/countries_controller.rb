class Api::V1::CountriesController < ApplicationController
  before_action :set_country, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_request!, except: [:index, :show]
  #before_action :require_login
  skip_before_action :verify_authenticity_token
  attr_reader :current_user

      # GET /countries
      # GET /countries.json
      # =begin
      # @api {get} /api/v1/countries List all countries
      # @apiVersion 0.3.0
      # @apiName GetCountries
      # @apiGroup Countries
      # @apiDescription get details of all countries.
      # @apiExample Example usage:
      # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -i http://localhost:3000/api/v1/countries
      #
      # @apiSuccess {Integer}    id                     Country-ID.
      # @apiSuccess {String}     short_code             Country Code.
      # @apiSuccess {String}     Full_Name              Country Name.
      # @apiSuccess {String}     Phone_code             Country Phone Code.
      # @apiSuccess {String}     Currency               Country Currency.
      # @apiSuccess {String}     language               Country Language.
      # @apiSuccess {Integer}    active                 Country Status.
      # @apiSuccess {Datetime}   created_at             Date of creating the Country.
      # @apiSuccess {Datetime}   updated_at             Date of updating the Country.
      #
      # @apiError NoAccessRight Only authenticated users can access the data.
      #
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Unauthorized
      #     {
      #       "error": "NoAccessRight"
      #    }
      # =end
  def index    
    @countries = Country.all
    respond_to do |format|   
         format.json { render json: @countries}  
    end
  end

      # GET /countries/1
      # GET /countries/1.json
      # =begin
      # @api {get} /api/countries/{:id} Get Country Data
      # @apiVersion 0.3.0
      # @apiName GetCountry
      # @apiGroup Countries
      # @apiDescription get details of specific country.
      # @apiExample Example usage:
      # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -i http://localhost:3000/api/v1/countries/1
      #
      # @apiSuccess {Integer}    id                     Country-ID.
      # @apiSuccess {String}     short_code             Country Code.
      # @apiSuccess {String}     Full_Name              Country Name.
      # @apiSuccess {String}     Phone_code             Country Phone Code.
      # @apiSuccess {String}     Currency               Country Currency.
      # @apiSuccess {String}     language               Country Language.
      # @apiSuccess {Integer}    active                 Country Status.
      # @apiSuccess {Datetime}   created_at             Date of creating the Country.
      # @apiSuccess {Datetime}   updated_at             Date of updating the Country.
      #
      # @apiError NoAccessRight Only authenticated users can access the data.
      # @apiError CountryNotFound   The <code>id</code> of the Country was not found.
      #
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Unauthorized
      #     {
      #       "error": "NoAccessRight"
      #    }
      # @apiErrorExample Error-Response:
      #     HTTP/1.1 404 Not Found
      #     {
      #       "error": "CountryNotFound"
      #    }
      # =end
  def show
    @country = Country.where("id =? " ,params[:id]).first
      respond_to do |format|
        format.json { render json: @country }
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def country_params
      params.require(:country).permit(:short_code, :Full_Name, :Phone_code, :Currency, :language, :active)
    end
end
