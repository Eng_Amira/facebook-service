json.extract! verification, :id, :user_id, :email_confirmation_token, :email_confirmed_at, :created_at, :updated_at
json.url verification_url(verification, format: :json)
