class EmailJob
    include SuckerPunch::Job
  
    # The perform method is in charge of our code execution when enqueued.
    def perform(params)
        MailService.new(params).call
    end
  
end