class SmsJob
    include SuckerPunch::Job
  
    # The perform method is in charge of our code execution when enqueued.
    def perform(params)
        SmsService.new(params).send_voice_msg
    end
  
end