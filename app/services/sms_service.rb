class SmsService
    require 'net/http'
    require 'uri'
    require 'json'

    def initialize(params)
      @tel = params[:tel]
      @applicationId = "451C42313BC234ABF92206EEA0796C1B"
      @messageId = "9A9EDA35B0552EF4E942BF272ADA1955"
      @text_uri = URI.parse("https://api.infobip.com/sms/1/text/single")
      @voice_uri = URI.parse("https://e66vq.api.infobip.com/tts/3/single")
      @pin_uri = URI.parse("https://e66vq.api.infobip.com/2fa/1/pin")
      #@resend_pin_uri = URI.parse("https://e66vq.api.infobip.com/2fa/1/pin/{pinId}/resend")
      #@verify_pin_uri = URI.parse("https://e66vq.api.infobip.com/2fa/1/pin/{pinId}/verify")
    end

    def send_text_msg        
        uri = @text_uri
        request = Net::HTTP::Post.new(uri)
        request.content_type = "application/json"
        request["Accept"] = "application/json"
        request["Authorization"] = "Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=="
        request.body = JSON.dump({
               "from" => "AmiraTest",
               "to" =>  @tel,
               "text" => "Test SMS."
                              })

        req_options = {
        use_ssl: uri.scheme == "https",
        }
        
        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
           @response = http.request(request) 
           @res= ActiveSupport::JSON.decode(@response.body) 
        end           
    end

    def send_voice_msg
        uri = @voice_uri
        request = Net::HTTP::Post.new(uri)
        request.content_type = "application/json"
        request["Accept"] = "application/json"
        request["Authorization"] = "Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=="
        request.body = JSON.dump({
               "from" => "AmiraTest",
               "to" => @tel,
               "text" => ",,Hello,,Test Voice message",
               "language" => "en"
                             })

        req_options = {
        use_ssl: uri.scheme == "https",
        }

        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
          @response = http.request(request) 
          @res= ActiveSupport::JSON.decode(@response.body)
        end
    end

    def send_pin_sms
        uri = @pin_uri
        request = Net::HTTP::Post.new(uri)
        request.content_type = "application/json"
        request["Host"] = "e66vq.api.infobip.com"
        request["Authorization"] = "Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=="
        request.body = JSON.dump({
               "applicationId" => @applicationId,
               "messageId" => @messageId,
               "from" => "AmiraTest",
               "to" => @tel            })

        req_options = {
        use_ssl: uri.scheme == "https",
           }

        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
           @response = http.request(request) 
           @res= ActiveSupport::JSON.decode(@response.body)
        end
    end

    def resend_pin_sms
        uri = @resend_pin_uri
        request = Net::HTTP::Post.new(uri)
        request.content_type = "application/json"
        request["Host"] = "e66vq.api.infobip.com"
        request["Authorization"] = "Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=="

        req_options = {
        use_ssl: uri.scheme == "https",
           }

        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
           @response = http.request(request) 
           @res= ActiveSupport::JSON.decode(@response.body)
        end
    end

    def verify_pin_sms
        uri = @verify_pin_uri
        request = Net::HTTP::Post.new(uri)
        request.content_type = "application/json"
        request["Host"] = "e66vq.api.infobip.com"
        request["Authorization"] = "Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=="
        request.body = JSON.dump({
               "pin" => "6088"
                              })

        req_options = {
        use_ssl: uri.scheme == "https",
           }

        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
           @response = http.request(request) 
           @res= ActiveSupport::JSON.decode(@response.body)
        end
    end


end



